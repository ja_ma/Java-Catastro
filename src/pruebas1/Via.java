package pruebas1;

public class Via {
	private String cv;
	private String tv;
	private String nv;
	
	public Via(String cv, String tv, String nv){
		this.cv = cv;
		this.tv = tv;
		this.nv = nv;
	}
	
	public String toString(){
		return cv + " " + tv + " " + nv;
	}

}
