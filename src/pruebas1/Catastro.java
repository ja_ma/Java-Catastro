package pruebas1;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

public class Catastro {
	
    String base_url = "http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC";

    public Map<String,String> getProvincias(){
    	
        String url = base_url + "/OVCCallejero.asmx/ConsultaProvincia";
		Map<String, String> map = new HashMap<String, String>();
		Document doc;
		try {
			doc = Jsoup.parse(new URL(url).openStream(), "UTF-8", "", Parser.xmlParser());

			Elements listItemsCpine = doc.select("cpine");
			Elements listItemsNp = doc.select("np");

			for (int i=0; i < listItemsCpine.size(); i++){
				map.put(listItemsCpine.get(i).text(),listItemsNp.get(i).text());
			}
		    //System.out.println(map);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return map;
    }
    
    public List<Municipio> getMunicipios(String provincia, String municipio){
    	if (municipio == null) {municipio = "";};
    	
        String url = base_url + "/OVCCallejero.asmx/ConsultaMunicipio";
        //url = url + "?Provincia="+provincia+"&Municipio="+municipio;
        url = url + String.format("?Provincia=%s&Municipio=%s",provincia,municipio);
		List<Municipio> listMunicipios = new ArrayList<Municipio>();
		Document doc;
		try {
			doc = Jsoup.parse(new URL(url).openStream(), "UTF-8", "", Parser.xmlParser());

			Elements nm = doc.select("nm");
			Elements cp = doc.select("cp");
			Elements cm = doc.select("cm");

			for (int i=0; i < nm.size(); i++){
				listMunicipios.add(new Municipio(nm.get(i).text(), cp.get(i).text(), cm.get(i).text()));
			}
		    //System.out.println(map);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listMunicipios;
    }
    
    public List<Via> getVias(String provincia, String municipio, String tipoVia, String nombreVia){
    	if (tipoVia == null) {tipoVia = "";};
    	if (nombreVia == null) {nombreVia = "";};
    	    	
    	String url = base_url +"/OVCCallejero.asmx/ConsultaVia";
    	url = url + String.format("?Provincia=%s&Municipio=%s&TipoVia=%s&NombreVia=%s",provincia,municipio,tipoVia,nombreVia);
    
    	List<Via> listVias = new ArrayList<Via>();
    	
		Document doc;
		try {
			doc = Jsoup.parse(new URL(url).openStream(), "UTF-8", "", Parser.xmlParser());

			Elements cv = doc.select("cv");
			Elements tv = doc.select("tv");
			Elements nv = doc.select("nv");

			for (int i=0; i < cv.size(); i++){
				listVias.add(new Via(cv.get(i).text(), tv.get(i).text(), nv.get(i).text()));
			}
		    //System.out.println(map);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	return listVias;
    }

}
