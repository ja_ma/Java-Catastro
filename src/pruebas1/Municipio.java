package pruebas1;

public class Municipio {
	
	private String municipio;
	private String codProv;
	private String codMun;
	
	public Municipio(String municipio, String codProv, String codMun){
		this.municipio = municipio;
		this.codProv = codProv;
		this.codMun = codMun;
	}
	
	public String toString(){
		return municipio + " " + codProv + " " + codMun;
	}
	
}
