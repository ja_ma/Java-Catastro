package pruebas1;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import javax.swing.JLabel;
import javax.swing.UIManager.*;



public class pruForm1 {

	private JFrame frame;
	private JTextField textField;
	private JTextField tb_Direccion;
	private JTextField tb_Ano;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pruForm1 window = new pruForm1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public pruForm1() {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        //String landf = "Nimbus";
		        String landf = "GTK+";
		    	if (landf.equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		textField = new JTextField();
		textField.setBounds(0, 0, 172, 38);
		textField.setColumns(10);

		//textField.setText("www.google.es");
		textField.setText("39.468227, -0.358011");

		JButton btnNewButton = new JButton("Catastro");
		btnNewButton.setBounds(184, 6, 96, 25);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Se ha escrito: " + textField.getText());

				//Document doc;
				//try {
				System.out.println(catastro());
				//} catch (IOException e1) {
				// TODO Auto-generated catch block
				//	e1.printStackTrace();
				//}

				Catastro cat = new Catastro();
				
				MapToConsole(cat.getProvincias());
				
				List<Municipio> listMunicipios = cat.getMunicipios("Valencia",null);
				for (int i=0; i < listMunicipios.size(); i++){
					System.out.println(listMunicipios.get(i).toString());
				}
				
				List<Via> listVias = cat.getVias("Valencia", "Alfafar", null, "ALC");
				for (int i=0; i < listVias.size(); i++){
					System.out.println(listVias.get(i).toString());
				}

			}
		});

		JLabel lblDireccin = new JLabel("Dirección");
		lblDireccin.setBounds(10, 50, 65, 15);

		JLabel lblAoao = new JLabel("Año");
		lblAoao.setBounds(10, 77, 27, 15);

		tb_Ano = new JTextField();
		tb_Ano.setBounds(93, 77, 56, 25);
		tb_Ano.setEditable(false);
		tb_Ano.setColumns(10);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(textField);
		frame.getContentPane().add(btnNewButton);
		frame.getContentPane().add(lblDireccin);
		
				tb_Direccion = new JTextField();
				tb_Direccion.setBounds(93, 50, 352, 25);
				tb_Direccion.setEditable(false);
				tb_Direccion.setColumns(10);
				frame.getContentPane().add(tb_Direccion);
		frame.getContentPane().add(lblAoao);
		frame.getContentPane().add(tb_Ano);



	}

	private String navegar(String url){

		String resultado = null;

		url = "http://"+url;

		Document doc;
		try {
			doc = Jsoup.connect(url).get();
			Element link = doc.select("a").first();
			String relHref = link.attr("href"); // == "/"
			String absHref = link.attr("abs:href"); // "http://jsoup.org/"

			resultado = absHref;

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return resultado;
	}

	private String catastro(){
		// 7321401YJ2772A0001UZ
		String coords = null;
		coords = "39.467421,-0.358615";
		coords = "39.467768, -0.358250"; 
		coords = "39.467693, -0.357995"; // edif
		coords = "39.468227, -0.358011"; // edif
		coords = "39.469271, -0.358789"; // edif
		coords = "39.469586, -0.360323"; // edif
		coords = textField.getText();

		tb_Direccion.setText("");
		tb_Ano.setText("");

		String aCoords[] = coords.replaceAll("\\s", "").split(",");
		String SRS = "EPSG:4326";
		String urlCatastroCoordenadasDistancia = "http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCoordenadas.asmx/Consulta_RCCOOR_Distancia?SRS=%s&Coordenada_X=%s&Coordenada_Y=%s";
		String urlCatastroCoordenadas = "http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCoordenadas.asmx/Consulta_RCCOOR?SRS=%s&Coordenada_X=%s&Coordenada_Y=%s";
		String urlCatastroDatosInmueble = "http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/Consulta_DNPRC?Provincia=%s&Municipio=%s&RC=%s";
		String urlFormateada = String.format(urlCatastroCoordenadas, SRS, aCoords[1], aCoords[0]);


		try
		{
			String url = urlFormateada;
			System.out.println(url);

			Map<String,String> aPC1PC2 = getPC1PC2(url);

			tb_Direccion.setText(aPC1PC2.get("dir"));
			// Get its attribute "genre"
			//String genre = movieFromXml.attr("genre");
			// Print the result
			//System.out.println(genre);


			String provincia = "Valencia";
			String municipio = "Valencia";
			String RC = aPC1PC2.get("pc1")+aPC1PC2.get("pc2");
			System.out.println(RC);
			String urlCatastroDatosInmuebleFormateada = String.format(urlCatastroDatosInmueble, provincia, municipio, RC);
			System.out.println(urlCatastroDatosInmuebleFormateada);
			Boolean isBuilding = false;
			Document docC = Jsoup.parse(new URL(urlCatastroDatosInmuebleFormateada).openStream(), "UTF-8", "", Parser.xmlParser());
			try {
				System.out.println("Año construcción: " + docC.select("ant").first().text());
				tb_Ano.setText(docC.select("ant").first().text());
			} catch (Exception e){
				e.printStackTrace();
				tb_Ano.setText("----");
				isBuilding = true;
				url = urlCatastroDatosInmuebleFormateada;
			}

			if (isBuilding == true){
				System.out.println("----- ES UN EDIFICIO Y BUSCAMOS EL AÑO EN EL PRIMER ELEMETO DEL EDIFICIO -----");
				System.out.println(url);
				aPC1PC2 = getPC1PC2(url);
				RC = aPC1PC2.get("pc1")+aPC1PC2.get("pc2")+aPC1PC2.get("car")+aPC1PC2.get("cc1")+aPC1PC2.get("cc2");
				System.out.println(RC);
				urlCatastroDatosInmuebleFormateada = "http://"+String.format(urlCatastroDatosInmueble, provincia, municipio, RC);
				System.out.println(urlCatastroDatosInmuebleFormateada);
				docC = Jsoup.parse(new URL(urlCatastroDatosInmuebleFormateada).openStream(), "UTF-8", "", Parser.xmlParser());
				System.out.println("Año construcción: " + docC.select("ant").first().text());
				tb_Ano.setText("E-"+docC.select("ant").first().text());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}




		return urlFormateada;
	}
	
	private Map<String, String> getPC1PC2(String url){
		Map<String, String> map = new HashMap<String, String>();

		Document docC;
		try {
			docC = Jsoup.parse(new URL(url).openStream(), "UTF-8", "", Parser.xmlParser());
			//System.out.println(docC.toString());
			Element elpc1 = docC.select("pc1").first();
			Element elpc2 = docC.select("pc2").first();
			Element dir = docC.select("ldt").first();
			Element car = docC.select("car").first();
			Element cc1 = docC.select("cc1").first();
			Element cc2 = docC.select("cc2").first();
			map.put("pc1",elpc1.text());
			map.put("pc2", elpc2.text());
			if (dir != null) {
				map.put("dir", dir.text());
			}
			if (car != null) {
				map.put("car", car.text());
			}
			if (cc1 != null) {
				map.put("cc1", cc1.text());
			}
			if (cc2 != null) {
				map.put("cc2", cc2.text());
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MapToConsole(map);

		return map;
	}
	
	public void MapToConsole(Map<String,String> map){
		Map<String, String> reversedMap = new TreeMap<String, String>(map);

		for (Map.Entry entry : reversedMap.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
	}
}
